package com.estoque.estoque.api;

import com.estoque.estoque.domain.Estoque;
import com.estoque.estoque.domain.EstoqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/estoque")
public class EstoqueController {
    @Autowired
    private EstoqueService service;

    @GetMapping
    public ResponseEntity<List<Estoque>> findAllList(){
        return ResponseEntity.ok().body(service.findAllList());
    }
}
