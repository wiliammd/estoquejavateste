package com.estoque.estoque.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstoqueService {
    @Autowired
    private EstoqueRepository repository;

    public List<Estoque> findAllList(){
        return repository.findAll();
    }
}
